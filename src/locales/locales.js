import zhCNLocale from "./lang/zh";
import enLocale from "./lang/en";

const locales = {
  "zh": zhCNLocale,
  en: enLocale
};

export default locales;
