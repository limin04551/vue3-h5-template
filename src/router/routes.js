import Layout from "@/layout/index.vue";

const routes = [
  {
    path: '/',
    name: 'root',
    component: Layout,
    children: [
      {
        path: "/",
        name: "home",
        component: () => import('../views/home/index.vue'),
        meta: {
          title: "首页",
          keepAlive: true
        }
      },
      {
        path: "tools",
        name: "tools",
        component: () => import('../views/tools/index.vue'),
        meta: {
          title: "请求工具",
          keepAlive: true
        }
      },
      {
        path: "about",
        name: "about",
        component: () => import('../views/about/index.vue'),
        meta: {
          title: "关于",
          keepAlive: true
        }
      },
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login.vue')
  },
  {
    path: '/:catchAll(.*)',
    name: 'not-found',
    component: () => import('../layout/404.vue'),
    meta: {
      title: "访问页面不存在",
      keepAlive: true
    }
  },
]
export default routes;
