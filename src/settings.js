import pkg from "../package.json";
export const pageDefaultTitle = pkg.name || "Vue3 H5 Template";
export const tokenKey = "app_token";
// cookie key 前缀
export const keyPrefix = "vue3_h5_template_";
// store key 前缀
export const storeKeyPrefix = "vue3_h5_template_";
